<?php

namespace App\Models;

class Validation
{
    /**
     * @param array $data
     *
     * @return array
     */
    public static function run(array &$data): array
    {
        $result = [];
        $data = self::xss($data);

        if (empty($data['name']) || empty($data['phone']) || empty($data['message']) || empty($data['save_type'])) {
            $result['error'][] = 'Не заполнено одно из или несколько обязательных полей!';
        }

        $phone_format = array(
            'options' => array("regexp" => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/')
        );
        if (!empty($data['phone']) && empty(filter_var($data['phone'], FILTER_VALIDATE_REGEXP, $phone_format)))
            $result['error'][] = 'Неверный формат номера телефона!';

        return $result;
    }

    /**
     * @param array|string $data
     *
     * @return array|string
     */
    public static function xss($data)
    {
        if (is_array($data))
            return array_map('self::xss', $data);

        return htmlspecialchars($data, ENT_QUOTES);
    }
}