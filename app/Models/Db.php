<?php

namespace App\Models;

use Exception;
use PDO;

class Db
{
    /** @var */
    private static $instance;

    /** @var PDO */
    private $pdo;

    /**
     * Db constructor.
     *
     * @throws Exception
     */
    private function __construct()
    {
        $file = dirname(__DIR__) . '/config.php';

        if (!is_file($file)) {
            throw new Exception('Файл с настройками доступа к базе данных не найден!');
        }

        $dbOptions = (require $file)['db'];

        try {
            $this->pdo = new PDO(
                'mysql:host=' . $dbOptions['host'] . ';dbname=' . $dbOptions['dbname'],
                $dbOptions['user'],
                $dbOptions['password']
            );
            $this->pdo->exec('SET NAMES UTF8');
        } catch (\PDOException $e) {
            throw new DbException('Ошибка при подключении к базе данных: ' . $e->getMessage());
        }
    }

    /**
     * @return static
     * @throws Exception
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $sql
     * @param array  $params
     *
     * @return array|null
     */
    public function query(string $sql, $params = []): ?array
    {
        $sth = $this->pdo->prepare($sql);
        $result = $sth->execute($params);

        if ($result === false) {
            return null;
        }

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return int
     */
    public function getLastInsertId(): int
    {
        return (int) $this->pdo->lastInsertId();
    }
}