<?php

namespace App\Models;

class File
{
    public function save(array $data): bool
    {
        $path = 'uploads/' . date('d.m.Y', time()) . '.txt';

        if (!$fp = fopen($path, 'a+'))
            return false;

        $message = $data['name'] . ' | ' . $data['phone'] . ' | ' . $data['message'] . PHP_EOL;

        flock($fp, LOCK_EX);
        fwrite($fp, $message);
        flock($fp, LOCK_UN);
        fclose($fp);

        return true;
    }
}