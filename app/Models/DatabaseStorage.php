<?php

namespace App\Models;

class DatabaseStorage implements StorageInterface
{
    protected $class;

    public function __construct()
    {
        $this->class = new Database();
    }

    public function save(array $data): bool
    {
        return (bool) $this->class->save($data);
    }
}