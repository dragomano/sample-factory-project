<?php

namespace App\Models;

class FileStorage implements StorageInterface
{
    protected $class;

    public function __construct()
    {
        $this->class = new File();
    }

    public function save(array $data): bool
    {
        return (bool) $this->class->save($data);
    }
}