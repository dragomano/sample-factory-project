<?php

namespace App\Models;

interface StorageInterface
{
    public function save(array $data): bool;
}