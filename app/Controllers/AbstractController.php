<?php

namespace App\Controllers;

use App\Views\View;

abstract class AbstractController
{
    /** @var View */
    protected $view;

    public function __construct()
    {
        $this->view = new View(__DIR__ . '/../../templates');
        $this->view->setVar('route', $_SERVER['REQUEST_URI']);
    }
}