<?php

namespace App\Controllers;

use App\Models\{DatabaseStorage, FileStorage, Validation};

class MainController extends AbstractController
{
    public function mainAction(): void
    {
        $this->view->render('main');
    }

    public function addAction(): void
    {
        $postData = json_decode(file_get_contents('php://input'), true);

        if (empty($postData))
            die;

        unset($postData['result']);

        $result = Validation::run($data = $postData);

        if (empty($result['error'])) {
            if ($data['save_type'] == 'db') {
                $storage = new DatabaseStorage();
            } else {
                $storage = new FileStorage();
            }

            if ($storage->save($data)) {
                $result['success'] = 'Успешно сохранено';
            } else {
                $result['error'][] = 'Ошибка сохранения';
            }
        }

        exit(json_encode($result));
    }
}
