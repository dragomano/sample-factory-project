require('./bootstrap');

window.Vue = require('vue');

Vue.component('form-appeal', require('./components/FormAppeal.vue').default);

const app = new Vue({
    el: '.container'
});
