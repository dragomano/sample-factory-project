# Тестовое задание

Cделать форму обратной связи с выбором куда отправлять заявку (текстовый файл, mysql) и _возможностью добавлять места для хранения заявок_ (использовать **паттерн фабрика**).
**Поля**: _имя_, _телефон_, само _обращение_.

## Что необходимо использовать:
* PHP 7
* Composer
* ООП (для создания заявки и места для хранения заявки)
* Фронтенд (желательно vuejs)

### Вид формы
![Реализация](screenshot.png)