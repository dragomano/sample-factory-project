<?php

session_start();

$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";
$app_path = preg_replace('#[^/]+$#', '', $app_path);
define('APP_URL', $app_path);
define('ROOT', dirname(__DIR__));

require ROOT . '/vendor/autoload.php';

try {
    $route = $_GET['q'] ?? '';
    $routes = require ROOT . '/app/routes.php';

    $isRouteFound = false;
    foreach ($routes as $pattern => $controllerAndAction) {
        preg_match($pattern, $route, $matches);

        if (!empty($matches)) {
            $isRouteFound = true;
            break;
        }
    }

    if (!$isRouteFound) {
        throw new \Exception('Маршрут не найден!');
    }

    unset($matches[0]);

    $controllerName = $controllerAndAction[0];
    $actionName = $controllerAndAction[1] . 'Action';

    $controller = new $controllerName();
    $controller->$actionName(...$matches);
} catch (Exception $e) {
    echo $e->getMessage();
}
