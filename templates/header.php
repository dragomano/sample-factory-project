<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title ?? 'Демонстрационный проект' ?></title>
    <link href="<?= APP_URL ?>css/app.css" rel="stylesheet">
</head>
<body class="bg-light">
    <div class="container">